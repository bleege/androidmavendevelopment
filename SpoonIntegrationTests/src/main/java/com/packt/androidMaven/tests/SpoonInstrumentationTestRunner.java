/**
 * @author Brad Leege <bleege@gmail.com>
 * Created on 12/7/14 at 10:18 PM
 */

package com.packt.androidMaven.tests;

import android.app.Application;
import android.app.KeyguardManager;
import android.os.PowerManager;
import android.test.InstrumentationTestRunner;

public class SpoonInstrumentationTestRunner extends InstrumentationTestRunner
{
    @Override
    public void onStart() {
        runOnMainSync(new Runnable() {
            @Override
            public void run() {
                Application app = (Application) getTargetContext().getApplicationContext();
                String simpleName = SpoonInstrumentationTestRunner.class.getSimpleName();
                ((KeyguardManager) app.getSystemService(KEYGUARD_SERVICE))
                        .newKeyguardLock(simpleName)
                        .disableKeyguard();
                ((PowerManager) app.getSystemService(POWER_SERVICE))
                        .newWakeLock(FULL_WAKE_LOCK | AC-QUIRE_CAUSES_WAKEUP | ON_AFTER_RELEASE, simpleName)
                        .acquire();
            }
        });

        super.onStart();
    }
}

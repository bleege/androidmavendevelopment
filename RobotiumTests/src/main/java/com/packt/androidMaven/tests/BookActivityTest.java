/**
 * @author Brad Leege <bleege@gmail.com>
 * Created on 12/7/14 at 10:49 PM
 */

package com.packt.androidMaven.tests;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import com.packt.androidMaven.AuthorActivity;
import com.packt.androidMaven.BookActivity;
import com.robotium.solo.Solo;

public class BookActivityTest extends ActivityInstrumentationTestCase2<BookActivity>
{

    private Solo solo;

    public BookActivityTest()
    {
        super(BookActivity.class);
    }

    @Override
    public void setUp() throws Exception
    {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception
    {
        solo.finishOpenedActivities();
    }

    @SmallTest
    public void testClickButton() throws Exception
    {
        solo.clickOnButton("View Author Name!");
        if (solo.waitForActivity(AuthorActivity.class))
        {
            assertTrue(solo.searchText("Patroklos Papapetrou"));
        }
        else
        {
            fail("No author activity started!");
        }
    }
}
